#include "ofApp.h"
#include "ofxCv.h"
#include "ofBitmapFont.h"


//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(true);

        grabber.setDeviceID(0);
        grabber.initGrabber(640,480);
        // To do flip video
        video = &grabber;

    
    //aruco.setThreaded(false);
    aruco.setup("intrinsics.int", video->getWidth(), video->getHeight());
    //aruco.getBoardImage(board.getPixels());
    //board.update();
    
    showMarkers = true;
    showBoard = false;
    showBoardImage = false;
    
    ofEnableAlphaBlending();
    ofDisableArbTex();
    
    //CreateAnim1
    for (int i=0; i<13; i++)
    {
        ofImage image;
        //image.load("of.png");
        string pic = "vanCleef-"+to_string(i)+".png";
        cout << "id = " << pic << endl;
        image.load("vanCleef/"+pic);
        
        image.getTexture().setTextureWrap( GL_REPEAT, GL_REPEAT );
        
        Anim1[i] = image;
    }
    indexAnimation =0;
    intervalleImages = 0;
    masque.set(0.7, 0.7);
    
    
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
    video->update();
    if(video->isFrameNew()){
        aruco.detectBoards(video->getPixels());
    }

}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255);
    
    video->draw(0,0);
    
    //aruco.draw();
    
    if(showMarkers){
        for(int i=0;i<aruco.getNumMarkers();i++){
            aruco.begin(i);
            drawmarker(0.15,ofColor::white);
            aruco.end();
        }
    }

    ofDrawBitmapString("markers detected: " + ofToString(aruco.getNumMarkers()),20,20);
    ofDrawBitmapString("fps " + ofToString(ofGetFrameRate()),20,40);
    ofDrawBitmapString("m toggles markers",20,60);
    ofDrawBitmapString("b toggles board",20,80);
    ofDrawBitmapString("i toggles board image",20,100);
    ofDrawBitmapString("s saves board image",20,120);
    ofDrawBitmapString("0-9 saves marker image",20,140);
    ofDrawBitmapString("intervalle Images"+ ofToString(intervalleImages),20,160);
    ofDrawBitmapString("index Animation"+ ofToString(indexAnimation),20,180);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key=='m') showMarkers = !showMarkers;
    if(key=='b') showBoard = !showBoard;
    if(key=='i') showBoardImage = !showBoardImage;
    if(key=='s') board.save("boardimage.png");
    if(key>='0' && key<='9'){
        // there's 1024 different markers
        int markerID = key - '0';
        aruco.getMarkerImage(markerID,240,marker);
        marker.save("marker"+ofToString(markerID)+".png");
    }

}

void ofApp::drawmarker(float size, const ofColor & color){
    
    ofDrawAxis(size);
    ofPushMatrix();
    
    // move up from the center by size*.5
    // to draw a box centered at that point
    ofTranslate(0,size*0.5,0.25);
    ofRotateX(90);
    
    masque.setPosition(0,0,0);
    Anim1[indexAnimation].getTexture().bind();
    masque.draw();
    Anim1[indexAnimation].getTexture().unbind();
    
    ofPopMatrix();
    


    if (intervalleImages <10)
    {
        intervalleImages++;
    }else{
        intervalleImages=0;
    }
    
    if (indexAnimation == 12 && intervalleImages == 10)
    {
        indexAnimation =0;
    }
    
    if (indexAnimation<13 && intervalleImages == 10)
    {
        indexAnimation++;
    }
    
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
